library(xaringanthemer)

## simple blue green light theme
blue_green_simple <- function() {
  mono_light(
    base_color = "#5D4D7A",
    header_font_google = google_font("Josefin Sans"),
    text_font_google   = google_font("Montserrat", "300", "300i"),
    code_font_google   = google_font("Droid Mono")
  )
}

## simple spacemacs dark theme
spacemacs_dark <- function() {
  mono_dark(
    base_color = "#2C9372",
    header_font_google = google_font("Josefin Sans"),
    text_font_google   = google_font("Montserrat", "300", "300i"),
    code_font_google   = google_font("Droid Mono"),
    background_color = "#292B2E",
    extra_css = list("h2" = list("color" = "#BB6DC4"),
                     "h3" = list("color" = "#4D92CC"),
                     ".title-slide h1" = list(color = "#2C9372"),
                     ".title-slide h2" = list(color = "#BB6DC4",
                                              "font-family" = "Montserrat"),
                     ".title-slide h3" = list(color = "#4D92CC",
                                              "font-family" = "Montserrat"),
                     ".write" = list("text-shadow" = "0 0 2px #BC2F5C, 0 0 15px #BC2F5C"),
                     ".mark" = list("background-color" = "yellow")
                     ),
    title_slide_background_color = "#292B2E",
    text_color = "#FFFFFF"
  )
}

## simple spacemacs light theme
spacemacs_light <- function(...) {
  mono_light(
    base_color = "#3FA5E3",
    header_font_google = google_font("Josefin Sans"),
    text_font_google   = google_font("Montserrat", "300", "300i"),
    code_font_google   = google_font("Droid Mono"),
    background_color = "#FBF8EF",
    extra_css = list("h2" = list("color" = "#BC2F5C"),
                     "h3" = list("color" = "#4FB99A"), ## TODO remove this at some point; this was a proof of
                     ## concept test of combining increment with mark; turns out
                     ## that it looks ugly because the highlight goes to the end
                     ## of the div and doesn't just highlight the text
                     "h4" = list("color" = "#3FA5E3",
                                 "font-size" = "28px"),
                     ".title-slide h2, .transition h2" = list(color = "#BC2F5C",
                                              "font-family" = "Montserrat"),
                     ".title-slide h3, .transition h3" = list(color = "#4FB99A",
                                              "font-family" = "Montserrat"),
                     ".write" = list("text-shadow" = "0px 0px 8px #DB7092, 0px 0px 20px #DB7092"),
                     ".large pre, .large code" = list("font-size" = "110%"),
                     ".large" = list("font-size" = "110%"),
                     ".small" = list("font-size" = "70%"),
                     ".strong" = list("font-weight" = "bold"),
                     ".mark" = list("background-color" = "#f5d6e0"),
                     ".mark2" = list("background-color" = "#c8e9df"),
                     ".big" = list("font-size" = "20px"),
                     ".transition h1, .transition h2, .transition h3, .transition h4" = list("padding-top" = "150px",
                                                                                             "font-size" = "55px"),
                     ".transition h2" = list(color = "#3FA5E3"),
                     ".transition h4" = list(color = "#BC2F5C",
                                             "font-family" = "Montserrat"),
                     ".inverse h1" = list(color = "#BB6DC4"),
                     ".inverse h2" = list(color = "#4D92CC"),
                     ".inverse h3" = list(color = "#2C9372"),
                     ".inverse" = list("background-color" = "#292B2E"),
                     ".formula" = list("font-size" = "200%",
                                       "text-align" = "center")
                     ),
    title_slide_background_color = "#292B2E",
    text_color = "#292B2E",
    ...
  )
}

spacemacs_light_bigger_text <-  function() {
  spacemacs_light(
    text_font_size = "30px"
  )
}

## monokai theme here!
